﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ASEB_2;

namespace UnitTestProjectASEB_B
{

    [TestClass]
    public class UnitTest1 //Unit Test
    {
        Form1 Form = new Form1(); //Instantiates Form1

        [TestMethod]
        public void TestMethod1() //Test Method to see if speedTotal is equal to 0.0
        {
            double acutal_result = Form1.speedTotal;
            double expected_result = 0.0;
            Assert.AreEqual(expected_result, acutal_result, "Correct");
        }

        [TestMethod]
        public void TestMethod2() //Test Method to see if total altitude, heart and power equal to 0
        {
            int acutal_result = Form1.altitudeTotal = Form1.heartTotal = Form1.powerTotal;
            int expected_result = 0;
            Assert.AreEqual(expected_result, acutal_result, "Correct");
        }

        [TestMethod]
        public void TestMethod3() //Test Method to see if date time and length processed are empty
        {
            string acutal_result = Form1.dateProcessed = Form1.timeProcessed = Form1.lengthProcessed;
            string expected_result = null;
            Assert.AreEqual(expected_result, acutal_result, "Correct");
        }
    }
}