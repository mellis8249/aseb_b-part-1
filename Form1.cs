﻿//Program to read data from HRM file and output results - Created by Mark Ellis : c3374267
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Globalization;

namespace ASEB_2
{
    public partial class Form1 : Form
    {
        //Static variables for totals
        public static double speedTotal;
        public static int heartTotal;
        public static int altitudeTotal;
        public static int powerTotal;

        //Static variables for max/min
        public static double minHeart = double.MaxValue;
        public static double maxHeart = double.MinValue;
        public static double minSpeed = double.MaxValue;
        public static double maxSpeed = double.MinValue;
        public static double minAltitude = double.MaxValue;
        public static double maxAltitude = double.MinValue;
        public static double minPower = double.MaxValue;
        public static double maxPower = double.MinValue;

        //Static variables for date, time and length of workout
        public static string dateProcessed;
        public static string timeProcessed;
        public static string lengthProcessed;
        public static double workoutLength;
        
        //Header variables
        public string Chosen_File = "";
        public string version1 = "";
        public string monitor1 = "";
        public string smode1 = "";
        public string date1 = "";
        public string time1 = "";
        public string length1 = "";
        public string interval1 = "";
        public string weight1 = "";

        //HRData variables
        private string HRM;
        private string line;
        private string heart;
        private string speed;
        private string cadence;
        private string altitude;
        private string power;
        private int counter;

        public Form1()
        {
            InitializeComponent();
            init();
        }

        public void init()
        {
            //Init Variables
            this.Width = 665; //Width of Form
            this.Height = 580; // Height of Form
            labelTitleHeader.Font = new Font(labelTitleHeader.Font.FontFamily, 15); //Sets label title font family and size
            labelTitleStatistics.Font = new Font(labelTitleStatistics.Font.FontFamily, 15); //Sets label title font family and size
            labelTitleHRData.Font = new Font(labelTitleHRData.Font.FontFamily, 15); //Sets label title font family and size
        }

        //Method to create datagridview columns
        public void dataGridColumns()
        {
            dataGridView1.AllowUserToAddRows = false; //Disallows users to add rows.

            //Datagrideview column for StartTime
            DataGridViewColumn time = new DataGridViewTextBoxColumn();
            time.HeaderText = "StartTime";
            int colIndex1 = dataGridView1.Columns.Add(time);

            //Dategrid view column for heart rate
            DataGridViewColumn heartrate = new DataGridViewTextBoxColumn();
            heartrate.HeaderText = "Heart Rate (bpm)";
            int colIndex2 = dataGridView1.Columns.Add(heartrate);

            //Datagrid view column for speed
            DataGridViewColumn speedkm = new DataGridViewTextBoxColumn();
            speedkm.HeaderText = "Speed (km/h)";
            int colIndex3 = dataGridView1.Columns.Add(speedkm);

            //Datagrid view column for cadencer
            DataGridViewColumn cadence = new DataGridViewTextBoxColumn();
            cadence.HeaderText = "Cadence";
            int colIndex4 = dataGridView1.Columns.Add(cadence);

            //Datagrid view column for altitude
            DataGridViewColumn altitude = new DataGridViewTextBoxColumn();
            altitude.HeaderText = "Altitude";
            int colIndex5 = dataGridView1.Columns.Add(altitude);

            //Datagrid view column for power
            DataGridViewColumn powerwatts = new DataGridViewTextBoxColumn();
            powerwatts.HeaderText = "Power (watts)";
            int colIndex6 = dataGridView1.Columns.Add(powerwatts);
        }

        //Method to open the HRM file
        public void openFile()
        {
            OpenFileDialog openFile = new OpenFileDialog(); //Opens the file selected by the user
            openFile.Title = "Choose File"; //Sets title
            openFile.FileName = ""; //Sets filename
            openFile.Filter = "HRM|*.hrm|Text Document|*.txt"; //Filters HRM files
            if (openFile.ShowDialog() != DialogResult.Cancel) //Checks if the open file dialog is not closed
            {

                Chosen_File = openFile.FileName; //Sets Chosen_File to the chosen file name
                readHeaderData(); //Calls the readHeaderData() method
                readHRData(); //Calls the ReadHRData() method
                toolStripStatusLabel1.Text = "File Loaded"; //Message
            }
        }

        //Method to read Header Data
        public void readHeaderData()
        {
            String[] lines = File.ReadAllLines(Chosen_File); // 

            //Version
            IEnumerable<String> version = from n in lines
                                       where n.StartsWith("Version")
                                       select n.Split('=')[1];
            foreach (String d in version) //
            {
                version1 = d; // 
            }

            //Monitor
            IEnumerable<String> monitor = from n in lines
                                       where n.StartsWith("Monitor")
                                       select n.Split('=')[1];
            foreach (String d in monitor)
            {
                monitor1 = d;
            }

            //SMode
            IEnumerable<String> smode = from n in lines
                                       where n.StartsWith("SMode")
                                       select n.Split('=')[1];
            foreach (String d in smode)
            {
                smode1 = d;
            }

            //Date
            IEnumerable<String> date = from n in lines
                                       where n.StartsWith("Date")
                                       select n.Split('=')[1];
            foreach (String d in date)
            {
                date1 = d;
            }

            //Time
            IEnumerable<String> time = from n in lines
                                       where n.StartsWith("StartTime")
                                       select n.Split('=')[1];
            foreach (String d in time)
            {
                time1 = d;
            }

            //Length
            IEnumerable<String> length = from n in lines
                                       where n.StartsWith("Length")
                                       select n.Split('=')[1];
            foreach (String d in length)
            {
                length1 = d;
            }

            //Interval
            IEnumerable<String> interval = from n in lines
                                       where n.StartsWith("Interval")
                                       select n.Split('=')[1];
            foreach (String d in interval)
            {
                interval1 = d;
            }

            //Weight
            IEnumerable<String> weight = from n in lines
                                       where n.StartsWith("Weight")
                                       select n.Split('=')[1];
            foreach (String d in weight)
            {
                weight1 = d;
            }

            //Date
            string dateString = date1; //Sets dateString
            string dateFormat = "yyyyMMdd"; //Sets a format for date
            DateTime myDate = DateTime.ParseExact(dateString, dateFormat, null); //myDate DateTime (uses dateString, dateFormat)
            dateProcessed = myDate.ToShortDateString(); //Stores myDate as a short date string in dateProcessed

            //Time
            string timeReplaced = time1.Replace(":", string.Empty); //Sets timeReplaced with time1 using replace to remove colons
            decimal timedec = Convert.ToDecimal(timeReplaced); //Can now convert timeReplaced to a decimal timedec
            int timeint = Convert.ToInt32(timedec); //Converts timedec and stores in timeint
            string forTimeAssembly = timeint.ToString(); //Converts timeint into forTimeAsssembly string
            string assembleTime = forTimeAssembly.Insert(2, ":");  //Sets assembleTime to forTimeAssembly and inserts a colon after second character
            string assembledTime = assembleTime.Insert(5, ":"); //Same as before but after the fifth character as well, stores in assembledTime
            string timeString = assembledTime; //Sets timeString to assembledTime 
            string timeFormat = "HH:mm:ss"; //Sets a format for time
            DateTime myTime = DateTime.ParseExact(timeString, timeFormat, null); //myTime DateTime (uses timeString, timeFormat)
            timeProcessed = myTime.ToLongTimeString(); //Stores myTime as a long time string in timeProcessed

            //Length
            string lengthReplaced = length1.Replace(":", string.Empty); //Sets lengthReplaced with length1 using replace to remove colons
            decimal lengthdec = Convert.ToDecimal(lengthReplaced); //Can now convert lengthReplaced to a decimal timedec
            int lengthint = Convert.ToInt32(lengthdec); //Converts lengthdec and stores in lengthint
            string testLength = lengthint.ToString();  //Converts lengthint into testLength string
            if (testLength.Length < 6) //Checks if testLength has less than 6 characters
            {

                testLength = lengthint.ToString().PadLeft(6, '0'); //Converts lengthint to string (adding a 0 before the first character using PadLeft)
            }

            string assembleLength = testLength.Insert(2, ":"); //Sets assembleLength to testLength and inserts a colon after second character
            string assembledLength = assembleLength.Insert(5, ":"); //Same as before but after the fifth character as well, stores in assembledLength
            string lengthString = assembledLength; //Sets lengthString to assembledLength 
            DateTime myLength = DateTime.ParseExact(lengthString, timeFormat, null); //myLength DateTime (uses lengthString, timeFormat)
            lengthProcessed = myLength.ToLongTimeString(); //Stores myLength as a long time string in lengthProcessed
            workoutLength = TimeSpan.Parse(lengthProcessed).TotalHours; //Does something... IMPORTANT ;)

            //Display header information in textBox controls
            textBoxVersion.Text = version1;
            textBoxMonitor.Text = monitor1;
            textBoxSMode.Text = smode1;
            textBoxDate.Text = dateProcessed;
            textBoxTime.Text = timeProcessed;
            textBoxLength.Text = lengthProcessed;
            textBoxInterval.Text = interval1;
            textBoxWeight.Text = weight1;
        }

        //Method to read HRData
        public void readHRData()
        {
            string path = Chosen_File;  //Sets path to Chosen_File
            using (StreamReader sr = new StreamReader(path)) //Creates new SteamReader using the Chosen_File
            {
                HRM = null; //Sets string HRM to null
                while ((HRM = sr.ReadLine()) != null) //Reads through the file stores each line in HRM
                {
                    if (HRM.IndexOf("[HRData]") != -1) //Checks the string index of HRM for [HRData]
                    {
                        //Finds [HRData]
                        break;
                    }
                 }

                line = sr.ReadLine(); //Reads through the file stores each line in line
                double convertSpeed = 0.0; //Double for convertSpeed
                dataGridColumns(); //Calls the dataGridColumns() method
                counter = 0; //Sets counter to 0

                while (line != null) //Executes if line is not null
                {
                    counter++; //Increments counter

                    heart = line.Split('\t')[0]; //Splits line string (using tab) and stores characters in heart
                    int heartint = Convert.ToInt32(heart); //Converts string heart to integer heartint
                    heartTotal += heartint; //heartTotal = heartint
                    if (heartint < minHeart) minHeart = heartint; //Finds min value
                    if (heartint > maxHeart) maxHeart = heartint; //Finds max value

                    speed = line.Split('\t')[1]; //Splits line string (using tab) and stores characters in speed
                    int speedint = Convert.ToInt32(speed); //Converts string speed to integer speedint
                    convertSpeed = ((double)speedint / 10); //Converts speedint into double convertSpeed and divides by ten (calculates km/h)
                    speedTotal += convertSpeed;  //speedTotal = convertSpeed
                    if (convertSpeed < minSpeed) minSpeed = convertSpeed; //Finds min value
                    if (convertSpeed > maxSpeed) maxSpeed = convertSpeed; //Finds max value

                    cadence = line.Split('\t')[2]; //Splits line string (using tab) and stores characters in cadence
                    int cadenceint = Convert.ToInt32(cadence); //Converts string cadence to integer cadenceint

                    altitude = line.Split('\t')[3]; //Splits line string (using tab) and stores characters in altitude
                    int altitudeint = Convert.ToInt32(altitude); //Converts string altitude to integer altitudeint
                    altitudeTotal += altitudeint; //altitudeTotal = altitudeint
                    if (altitudeint < minAltitude) minAltitude = altitudeint; //Finds min value
                    if (altitudeint > maxAltitude) maxAltitude = altitudeint; //Finds max value

                    power = line.Split('\t')[4]; //Splits line string (using tab) and stores characters in power
                    int powerint = Convert.ToInt32(power); //Converts string power to integer powerint
                    powerTotal += powerint; //powerTotal = powerint
                    if (powerint < minPower) minPower = powerint; //Finds min value
                    if (powerint > maxPower) maxPower = powerint; //Finds max value

                    //Add rows the dataGridView
                    dataGridView1.Rows.Add(timeProcessed, heartint + " bpm", convertSpeed + " km/h", cadenceint, altitudeint, powerint);

                    line = sr.ReadLine(); //Reads through the file stores each line in line
                }
                outputStatistics(); //Calls the outputStatistics method
            }
        }

        //Method to output statistics
        public void outputStatistics()
        {
            //Set double variables for averages
            double speedAvg = 0.0;
            double altitudeAvg = 0.0;
            double heartAvg = 0.0;
            double powerAvg = 0.0;
            double totalDistance = 0.0;

            if (counter > 0) //Checks if counter is more than 0
            {

                speedAvg = speedTotal / counter; //speedAvg = speedTotal divided by counter
                heartAvg = heartTotal / counter; //heartAvg = heartTotal divided by counter
                altitudeAvg = altitudeTotal / counter; //altitudeAvg = altitudeTotal divided by counter
                powerAvg = powerTotal / counter; //powerAvg = powerTotal divided by counter
                totalDistance = workoutLength * speedAvg; //totalDistance = workoutLength multiplied by speedAvg
            }

            //Display for speed statistics
            textBoxAverageSpeed.Text = speedAvg.ToString("0.0") + " km/h";
            textBoxMinSpeed.Text = minSpeed.ToString("0.0") + " km/h";
            textBoxMaxSpeed.Text = maxSpeed.ToString("0.0") + " km/h";

            //Display for heart statistics
            textBoxAverageHeart.Text = heartAvg.ToString() + " bpm";
            textBoxMinHeart.Text = minHeart.ToString() + " bpm";
            textBoxMaxHeart.Text = maxHeart.ToString() + " bpm";

            //Display for altitude statistics
            textBoxAverageAltitude.Text = altitudeAvg.ToString();
            textBoxMinAltitude.Text = minAltitude.ToString();
            textBoxMaxAltitude.Text = maxAltitude.ToString();

            //Display for power statistics
            textBoxAveragePower.Text = powerAvg.ToString();
            textBoxMinPower.Text = minPower.ToString();
            textBoxMaxPower.Text = maxPower.ToString();

            //Display for total distance statistic
            textBoxTotalDistance.Text = totalDistance.ToString("0.0") + " km";
        }

        //Method to clear static totals
        public void clearStaticTotals()
        {
        //Sets static totals variables to 0
        speedTotal = 0.0;
        heartTotal = 0;
        altitudeTotal = 0;
        powerTotal = 0;
        }

        //Method to clear contents of controls
        public void clearContents()
        {
            clearStaticTotals(); //Calls the clearStaticTotals() method

            //Clears header data textbox controls
            textBoxVersion.Text = textBoxMonitor.Text = textBoxSMode.Text = textBoxDate.Text = textBoxTime.Text = textBoxLength.Text = textBoxInterval.Text
            = textBoxWeight.Text = "";

            //Clears speed statistics controls
            textBoxAverageSpeed.Text = "";
            textBoxMinSpeed.Text = "";
            textBoxMaxSpeed.Text = "";

            //Clears heart rate statistics controls
            textBoxAverageHeart.Text = "";
            textBoxMinHeart.Text = "";
            textBoxMaxHeart.Text = "";
            
            //Clears altitude statistics controls
            textBoxAverageAltitude.Text = "";
            textBoxMinAltitude.Text = "";
            textBoxMaxAltitude.Text = "";

            //Clears power statistics controls
            textBoxAveragePower.Text = "";
            textBoxMinPower.Text = "";
            textBoxMaxPower.Text = "";
            
            //Clears totla distance statistic control
            textBoxTotalDistance.Text = "";

            //Clears dataGridView1 contents

            do //Start of do while loop
            {
                foreach (DataGridViewRow row in dataGridView1.Rows) //Iterates dataGridView1.Rows as rows
                {
                    try //Start of try catch loop
                    {
                        dataGridView1.Rows.Remove(row); //Removes rows from dataGridView1
                    }
                    catch (Exception) { } //Catches any exceptions
                }
            } while (dataGridView1.Rows.Count >= 1); //While dataGridView1 rows are more or equal to 1
        }

        //Select file menu item
        private void selectFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFile(); //Calls the openFile() method
        }

        //Reset menu item
        private void resetToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            clearContents(); //Calls the clearContents method
            toolStripStatusLabel1.Text = "Contents Cleared"; //Message
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "About: Program to read data from HRM file and output results - Created by Mark Ellis : c3374267";
        }

        //Exit menu item
        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit(); //Exits application
        }
    }
}
